import React, { useState } from 'react';
import { Line } from 'react-chartjs-2';
import { TabContent, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col } from 'reactstrap';

const CovidChart = (props) => {
  return (
    <div>
      <Row>
        <Col sm="12">
          <Line
            data={props.data}
          />
        </Col>
      </Row>
    </div>
  )
}

export default CovidChart;