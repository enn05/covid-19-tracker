import React, { Component } from 'react'
import MaterialTable from 'material-table';


class CovidTableMT extends Component {
  render() {
    return (
      <MaterialTable
        title="All Cases"
        columns={[
          {
            title: 'Case #',
            field: 'CaseCode'
          },
          {
            title: 'Age',
            field: 'Age'
          },
          {
            title: 'Gender',
            field: 'Sex'
          },
          {
            title: 'Health Status',
            field: 'HealthStatus',
          },
          {
            title: 'Date Confirmed',
            field: 'DateRepConf',
          },
          {
            title: 'Region',
            field: 'RegionRes',
          },
          {
            title: 'Province',
            field: 'ProvRes',
          },
          {
            title: 'Status',
            field: 'RemovalType',
          },
        ]}
        data={this.props.data}
        actions={[
          {
            icon: 'refresh',
            tooltip: 'Refresh Data',
            isFreeAction: true,
            onClick: () => this.props.refetch(),
          }
        ]}
        options={{
          search: true,
          cellStyle: {
            color: '#ffc107',
            backgroundColor: '#0C2339'
          },
          headerStyle: {
            backgroundColor: '#0C2339',
            color: '#ffc107'
          },
          pageSize: 10,
          pageSizeOptions: [10,20,50,100]
        }}
        isLoading={this.props.isLoading}
      />
    )
  }
}

export default CovidTableMT;