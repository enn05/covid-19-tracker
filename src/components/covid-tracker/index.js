import React, { useState, useEffect } from 'react';
import { Card, CardTitle, CardText, Row, Col } from 'reactstrap';
import { Spinner } from 'reactstrap';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import RefreshIcon from '@material-ui/icons/Refresh';
import CovidTableMT from './CovidTableMT';
import moment from 'moment';
import _ from 'lodash';
import CovidChart from './CovidChart';

const Covid = () => {
  const [allCases, setAllCases] = useState([]);
  const [casesPerRegion, setCasesPerRegion] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isLoadingTbl, setIsLoadingTbl] = useState(true);
  const [casesPerDay, setCasesPerDay] = useState([]);

  useEffect(() => {
    fetchAllCases()
    fetchCasesPerRegion()
  }, [])

  const fetchAllCases = () => {
    fetch('https://corona-api.com/countries/PH')
      .then(res => res.json())
      .then(data => {
        setAllCases(data.data.timeline[0])
        setIsLoading(false)
        const casesTimelines = data.data.timeline;
        let caseDates = [];
        let confirmed = [];
        let deaths = [];
        let recovered = [];
        casesTimelines.map(indivTimeline => {
          caseDates.unshift(moment(indivTimeline.date).format("MMM D"))
          confirmed.unshift(indivTimeline.new_confirmed)
          deaths.unshift(indivTimeline.new_deaths)
          recovered.unshift(indivTimeline.new_recovered)
        })
        setCasesPerDay({
          labels: caseDates,
          datasets: [
            {
              label: 'Deaths',
              data: deaths,
              backgroundColor: [
                'rgb(255, 0, 0)',
              ]
            },
            {
              label: 'Recovered',
              data: recovered,
              backgroundColor: [
                'rgb(0, 128, 0)',
              ],
            },
            {
              label: 'Confirmed',
              data: confirmed,
              backgroundColor: [
                'rgb(255, 255, 0)',
              ]
            },
          ]
        })
      })
  }

  const fetchCasesPerRegion = () => {
    setIsLoadingTbl(true)
    setCasesPerRegion([])
    fetch('https://covid19pinas.firebaseio.com/cases.json')
      .then(res => res.json())
      .then(data => {
        if (data) {
          setCasesPerRegion(data)
          setIsLoadingTbl(false)
        }
      })
  }

  const refresh = () => {
    setIsLoading(true)
    setCasesPerDay([])
    fetchAllCases()
  }

  return (
    <React.Fragment>
      <Container>
        <div className="d-flex align-items-center justify-content-center" style={{ padding: "30px 0 0 0" }}>
          <Row className="text-center">
            <Col sm="12">
              <h1>Philippines COVID-19 Cases</h1>
              <p>as of {moment(allCases.updated_at).format('MMMM Do YYYY, h:mm:ss a')}</p>
            </Col>
          </Row>
        </div>
        <Row style={{ padding: "30px 0 15px 0" }}>
          <Col sm="12" style={{ paddingBottom: "15px" }}>
            <Button variant="contained" color="primary" onClick={refresh} style={{ float: "right" }}>
              <RefreshIcon style={{ minWidth: "90px" }} />
            </Button>
          </Col>
          <Col sm="12" style={{ paddingBottom: "30px" }}>
            <Card body className="text-center" style={{ borderLeft: "3px solid orange" }}>
              <CardTitle>{isLoading ? <Spinner color="warning" /> : <h1>{allCases.confirmed}</h1>}</CardTitle>
              <CardText>
                <small className={isLoading ? null : "new-cases"}>{isLoading ? "" : `${allCases.new_confirmed} New Cases`}</small>
              </CardText>
              <CardText>TOTAL Cases</CardText>
            </Card>
          </Col>
          <Col sm="4">
            <Card body className="text-center" style={{ borderLeft: "3px solid yellow" }}>
              <CardTitle>{isLoading ? <Spinner color="warning" /> : <h1>{allCases.active}</h1>}</CardTitle>
              {isLoading ? null : <div className="empty-zone"></div>}
              <CardText>Active Cases</CardText>
            </Card>
          </Col>
          <Col sm="4">
            <Card body className="text-center" style={{ borderLeft: "3px solid red" }}>
              <CardTitle>{isLoading ? <Spinner color="warning" /> : <h1>{allCases.deaths}</h1>}</CardTitle>
              <CardText>
                <small className={isLoading ? null : "new-cases"}>{isLoading ? "" : `${allCases.new_deaths} New Deaths`}</small>
              </CardText>
              <CardText>Deaths</CardText>
            </Card>
          </Col>
          <Col sm="4">
            <Card body className="text-center" style={{ borderLeft: "3px solid green" }}>
              <CardTitle>{isLoading ? <Spinner color="warning" /> : <h1>{allCases.recovered}</h1>}</CardTitle>
              <CardText>
                <small className={isLoading ? null : "new-cases"}>{isLoading ? "" : `${allCases.new_recovered} New Recovered`}</small>
              </CardText>
              <CardText>Recovered</CardText>
            </Card>
          </Col>
        </Row>
        <div style={{ paddingBottom: "30px" }}>
          <Card body className="text-center">
            <CardTitle>
              <h3>Cases per Day</h3>
            </CardTitle>
            <CovidChart style={{ paddingBottom: "30px" }}
              data={casesPerDay}
            />
          </Card>
        </div>
        <div style={{ paddingBottom: "30px" }}>
          <Card body className="text-center">
            {casesPerRegion.length <= 0 ?
              <div style={{ padding: "15rem 0" }}>
                <CardTitle><Spinner color="warning" /></CardTitle>
                <CardText>Loading...</CardText>
              </div>
              :
              <CovidTableMT
                data={casesPerRegion}
                isLoading={isLoadingTbl}
                refetch={fetchCasesPerRegion}
              />
            }
          </Card>
        </div>
      </Container>
    </React.Fragment>
  )
}

export default Covid;