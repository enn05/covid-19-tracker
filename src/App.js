import React from 'react';
import './App.css';
import Particles from 'react-particles-js'
import Covid from './components/covid-tracker';

const particlesOption = {
  particles: {
    number: {
     value: 50,
     density: {
        enable: true,
        value_area: 300
     }
    },
  }
}

function App() {
  return (
    <React.Fragment>
      <Particles className='particles'
      params={particlesOption}
      />
      <Covid />
    </React.Fragment>
  );
}

export default App;
